import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:ui/components/trainer-avatar.dart';
import 'package:ui/models/run.dart';
import 'package:ui/models/user.dart';
import 'package:intl/intl.dart';
import 'package:flutterfire_ui/firestore.dart';

class RunListView extends StatefulWidget {
  const RunListView({
    super.key,
  });

  @override
  _RunListViewState createState() => _RunListViewState();
}

class _RunListViewState extends State<RunListView> {
  List<DateTime> _dateFilterList = [];
  DateTime _activeDate = DateTime.now();

  @override
  void initState() {
    super.initState();
    generateDateRange();
    setState(() {
      _activeDate = _dateFilterList[0];
    });
  }

  void generateDateRange() {
    final start = DateTime.now();
    setState(() {
      _dateFilterList = List.generate(
          14, (i) => DateTime(start.year, start.month, start.day + (i)));
    });
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        // Add the app bar to the CustomScrollView.
        SliverAppBar(
          actions: [
            IconButton(
              onPressed: () {
                print('test');
              },
              icon: const Icon(Icons.arrow_back),
            )
          ],
          // Provide a standard title.
          title: Text('Live Runs'),
          centerTitle: true,
          // Allows the user to reveal the app bar if they begin scrolling
          // back up the list of items.
          floating: true,
          // Display a placeholder widget to visualize the shrinking size.
          flexibleSpace: Placeholder(),
          // Make the initial height of the SliverAppBar larger than normal.
          expandedHeight: 100,
        ),
        // Next, create a SliverList
        SliverToBoxAdapter(
          child: Container(
            height: 100.0,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: _dateFilterList.length,
              itemBuilder: (context, index) {
                return Container(
                    width: 100.0,
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          _activeDate = _dateFilterList[index];
                        });
                      },
                      child: Card(
                        child: Column(children: [
                          Text(abbreviatedWeekdayFromDateTime(
                              _dateFilterList[index])),
                          Text(_dateFilterList[index].day.toString()),
                          Icon(_activeDate == _dateFilterList[index]
                              ? Icons.thumb_up
                              : Icons.thumb_down)
                        ]),
                      ),
                    ));
              },
            ),
          ),
        ),
        RunListStream(_activeDate),
      ],
    );
  }
}

String abbreviatedWeekdayFromDateTime(DateTime dt) {
  final weekdays = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
  return weekdays[dt.weekday - 1];
}

class RunListViewTile extends StatefulWidget {
  final Run run;

  const RunListViewTile(
    this.run, {
    super.key,
  });

  @override
  _RunListViewTileState createState() => _RunListViewTileState();
}

class _RunListViewTileState extends State<RunListViewTile> {
  User? _coach;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    fetchUserFromFirestore(widget.run.coachId!)
        .then((coach) => setState(() {
              _coach = coach;
            }))
        .catchError((err) => throw Exception(err));
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Text(DateFormat('h:mm a').format(widget.run.startsAt!),
          style: TextStyle(fontSize: 14, color: Colors.deepOrange)),
      ListTile(
        onTap: () => Navigator.of(context).pushNamed('/${widget.run.uid}'),
        leading: TrainerAvatar(photoUrl: _coach?.photoUrl),
        title: Text('${_coach?.firstName} ${_coach?.lastName}'),
        subtitle:
            Text('${widget.run.durationInMinutes} min - ${widget.run.title}'),
        trailing: Text.rich(TextSpan(
          children: [
            TextSpan(text: widget.run.userCount.toString()),
            WidgetSpan(child: Icon(Icons.people))
          ],
        )),
      )
    ]);
  }
}

class RunListStream extends StatelessWidget {
  final DateTime date;

  RunListStream(this.date);

  @override
  Widget build(BuildContext context) {
    return FirestoreQueryBuilder(
      query: FirebaseFirestore.instance
          .collection('runs')
          // TODO: Choose either trainer or coach and stick with it.
          .where('startsAt',
              isGreaterThanOrEqualTo: Timestamp.fromDate(this.date))
          .where('startsAt',
              isLessThanOrEqualTo:
                  Timestamp.fromDate(this.date.add(Duration(hours: 24))))
          .orderBy('startsAt')
          .withConverter<Run>(
              fromFirestore: (snapshot, _) => Run.fromSnapshot(snapshot),
              toFirestore: (run, _) => run.toJson()),
      builder: (context, snapshot, _) {
        if (snapshot.hasError) {
          throw Exception('could not fetch run list: ${snapshot.error}');
        }

        if (snapshot.isFetching) {
          return SliverToBoxAdapter(
              child: Center(
            child: CircularProgressIndicator(),
          ));
        }

        return SliverList(
          // Use a delegate to build items as they're scrolled on screen.
          delegate: SliverChildBuilderDelegate(
            // The builder function returns a ListTile with a title that
            // displays the index of the current item.
            (context, index) {
              return RunListViewTile(snapshot.docs[index].data() as Run);
            },
            // Builds 1000 ListTiles
            childCount: snapshot.docs.length,
          ),
        );
      },
    );

    // return StreamBuilder<QuerySnapshot>(
    //   stream: FirebaseFirestore.instance
    //       .collection('runs')
    //   // TODO: Choose either trainer or coach and stick with it.
    //       .where('startsAt', isGreaterThanOrEqualTo: Timestamp.fromDate(this.date))
    //       .where('startsAt', isLessThanOrEqualTo: Timestamp.fromDate(this.date.add(Duration(hours: 24))))
    //       .orderBy('startsAt')
    //       .withConverter<Run>(
    //         fromFirestore: (snapshot, _) => Run.fromSnapshot(snapshot),
    //         toFirestore: (run, _) => run.toJson()).snapshots(),
    //   builder: (context, snapshot) {
    //     if (snapshot.hasError) {
    //       throw Exception('could not fetch run list: ${snapshot.error}');
    //     }
    //
    //     if (snapshot.connectionState == ConnectionState.waiting) {
    //       return SliverToBoxAdapter(
    //           child: Center(
    //           child: CircularProgressIndicator(),
    //     ));
    //     }
    //
    //     return SliverList(
    //       // Use a delegate to build items as they're scrolled on screen.
    //       delegate: SliverChildBuilderDelegate(
    //         // The builder function returns a ListTile with a title that
    //         // displays the index of the current item.
    //             (context, index) {
    //           return RunListViewTile(snapshot.data?.docs[index] as Run);
    //         },
    //         // Builds 1000 ListTiles
    //         childCount: snapshot.data?.docs.length,
    //       ),
    //     );
    //   },
    // );
  }
}
