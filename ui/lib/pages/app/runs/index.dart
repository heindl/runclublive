import 'package:flutter/material.dart';
import './run-list.dart';
import './run-detail.dart';
import './run-chat-view.dart';

class RunsPage extends StatefulWidget {
  const RunsPage({
    super.key,
    required this.route,
  });

  final String route;

  @override
  _RunsPageState createState() => _RunsPageState();
}

class _RunsPageState extends State<RunsPage> {
  final _navigatorKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: _navigatorKey,
      initialRoute: widget.route,
      onGenerateRoute: generateRoute,
    );
  }

  static Route<dynamic> generateRoute(RouteSettings settings) {
    if (settings.name!.length <= 1) {
      return MaterialPageRoute(builder: (context) => RunListView());
    }

    final pathSections = settings.name!.substring(1).split("/");
    if (pathSections.length > 1 && pathSections[1] == "chat") {
      return MaterialPageRoute(
          builder: (context) => RunChatView(pathSections[0]));
    }

    return MaterialPageRoute(
        builder: (context) => RunDetailView(pathSections[0]));
  }
}
