import 'package:flutter/material.dart';
import 'package:flutterfire_ui/auth.dart';
import 'package:ui/pages/app/profile/editable_bio.dart';
import 'package:firebase_auth/firebase_auth.dart' show FirebaseAuth;
import 'package:flutter/cupertino.dart' hide Title;
import 'package:flutter/material.dart' hide Title;
import 'package:cloud_firestore/cloud_firestore.dart';

class ProfilePage extends StatefulWidget {
  ProfilePage({
    super.key,
    required this.queryParamUserID,
  });

  final String queryParamUserID;

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  Map<String, dynamic>? _userDocument;
  final auth = FirebaseAuth.instance;
  String? _userID;
  final firestoreInstance = FirebaseFirestore.instance;

  Future<void> updateUserDocument(String uid) async {
    if (uid == '') {
      throw new Exception('User ID is expected when querying user document');
    }

    var userDocument =
        await firestoreInstance.collection('users').doc(uid).get();

    Map<String, dynamic>? data;
    if (!userDocument.exists) {
      await firestoreInstance
          .collection('users')
          .doc(uid)
          .set({'role': 'user'});
      data = {};
    } else {
      data = userDocument.data();
    }
    print(data);
    setState(() {
      _userDocument = data;
    });
  }

  @override
  void initState() {
    super.initState();
    final userID = widget.queryParamUserID != ''
        ? widget.queryParamUserID
        : auth.currentUser?.uid;

    setState(() {
      _userID = userID;
    });
    updateUserDocument(userID ?? '');
  }

  @override
  Widget build(BuildContext context) {
    final content =
        Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
      Align(
        child: UserAvatar(
          auth: auth,
          // placeholderColor: avatarPlaceholderColor,
          // shape: avatarShape,
          // size: avatarSize,
        ),
      ),
      Align(child: EditableUserDisplayName(auth: auth)),
      Align(
          child: EditableUserBio(
              userID: _userID ?? '', userBio: _userDocument?['bio']))
    ]);

    final body = Padding(
      padding: const EdgeInsets.all(16),
      child: Center(
        child: LayoutBuilder(
          builder: (context, constraints) {
            if (constraints.maxWidth > 500) {
              return ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: 500),
                child: content,
              );
            } else {
              return content;
            }
          },
        ),
      ),
    );

    return SafeArea(child: SingleChildScrollView(child: body));
  }
}

// class _ProfilePageState extends State<ProfilePage> {
//
//   @override
//   Widget build(BuildContext context) {
//     if (FirebaseAuth.instance.currentUser != null) {
//       print('current user');
//       print(FirebaseAuth.instance.currentUser?.uid);
//     }
//
//     return
//         ProfileScreen(
//           actions: [
//             SignedOutAction((context) {
//               Navigator.pushReplacementNamed(context, AuthRouteManager.signInPage);
//             }),
//           ],
//           // actionCodeSettings: actionCodeSettings,
//         );
//   }
// }
