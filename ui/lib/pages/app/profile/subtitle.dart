import 'package:flutter/material.dart';

class Subtitle extends StatelessWidget {
  final String text;
  final FontWeight? fontWeight;

  const Subtitle({
    Key? key,
    required this.text,
    this.fontWeight,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: Theme.of(context)
          .textTheme
          .subtitle1!
          .copyWith(fontWeight: fontWeight),
    );
  }
}
