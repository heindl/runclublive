import 'package:flutter/material.dart';
import './trainer-list.dart';
import './trainer-detail.dart';

class TrainersPage extends StatefulWidget {
  const TrainersPage({
    super.key,
    required this.route,
  });

  final String route;

  @override
  _TrainersPageState createState() => _TrainersPageState();
}

class _TrainersPageState extends State<TrainersPage> {
  final _navigatorKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: _navigatorKey,
      initialRoute: widget.route,
      onGenerateRoute: generateRoute,
    );
  }

  static Route<dynamic> generateRoute(RouteSettings settings) {
    if (settings.name!.length <= 1) {
      return MaterialPageRoute(builder: (context) => TrainerListView());
    }
    final trainerId = settings.name!.substring(1);
    return MaterialPageRoute(
        builder: (context) => TrainerProfileView(trainerId));
  }
}
