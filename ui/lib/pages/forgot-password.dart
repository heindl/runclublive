import 'package:flutterfire_ui/auth.dart';
import 'package:flutter/material.dart';
import '../components/decorations.dart';

Widget getForgotPasswordPage(BuildContext context) {
  final arguments =
      ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>?;

  return ForgotPasswordScreen(
    email: arguments?['email'],
    headerMaxExtent: 200,
    headerBuilder: headerIcon(Icons.lock),
    sideBuilder: sideIcon(Icons.lock),
  );
}
