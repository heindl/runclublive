import 'package:flutterfire_ui/auth.dart';
import 'package:flutter/material.dart';
import '../routes/auth.dart';

Widget getSignInPage(BuildContext context) {
  return SignInScreen(
    actions: [
      ForgotPasswordAction((context, email) {
        Navigator.pushNamed(
          context,
          AuthRouteManager.forgotPasswordPage,
          arguments: {'email': email},
        );
      }),
      AuthStateChangeAction<SignedIn>((context, state) {
        if (!state.user!.emailVerified) {
          Navigator.pushNamed(context, AuthRouteManager.verifyEmailPage);
        } else {
          Navigator.pushReplacementNamed(context, AuthRouteManager.appFlow);
        }
      }),
    ],
    styles: const {
      EmailFormStyle(signInButtonVariant: ButtonVariant.filled),
    },
    // headerBuilder: headerImage('assets/images/flutterfire_logo.png'),
    // sideBuilder: sideImage('assets/images/flutterfire_logo.png'),
    subtitleBuilder: (context, action) {
      return Padding(
        padding: const EdgeInsets.only(bottom: 8),
        child: Text(
          action == AuthAction.signIn
              ? 'Welcome to RunClubLive! Please sign in to continue.'
              : 'Welcome to RunClubLive! Please create an account to continue',
        ),
      );
    },
    footerBuilder: (context, action) {
      return Center(
        child: Padding(
          padding: const EdgeInsets.only(top: 16),
          child: Text(
            action == AuthAction.signIn
                ? 'By signing in, you agree to our terms and conditions.'
                : 'By registering, you agree to our terms and conditions.',
            style: const TextStyle(color: Colors.grey),
          ),
        ),
      );
    },
  );
}
