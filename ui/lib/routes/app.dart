import 'package:flutter/material.dart';
import 'package:ui/pages/app/profile/index.dart';
import '../pages/app/browse/index.dart';
import '../pages/app/profile/index.dart';
import '../pages/app/trainers/index.dart';
import '../pages/app/runs/index.dart';
import '../pages/404.dart';

class AppRouteManager {
  static const String browsePage = '/';
  static const String profilePage = '/profile';
  static const String trainersPage = '/trainers';
  static const String runsPage = '/runs';

  static Route<dynamic> generateRoute(RouteSettings settings) {
    print(settings.name);
    switch (settings.name) {
      case browsePage:
        return MaterialPageRoute(builder: (context) => BrowsePage());
      default:
        if (settings.name!.startsWith(runsPage)) {
          final subroute = settings.name!.substring(runsPage.length);
          return MaterialPageRoute(
              builder: (context) => RunsPage(route: subroute));
        }
        if (settings.name!.startsWith(trainersPage)) {
          final subroute = settings.name!.substring(trainersPage.length);
          return MaterialPageRoute(
              builder: (context) => TrainersPage(route: subroute));
        }
        if (settings.name!.startsWith(profilePage)) {
          final userID = settings.name!.substring(profilePage.length);
          return MaterialPageRoute(
              builder: (context) => ProfilePage(queryParamUserID: userID));
        }
        return MaterialPageRoute(builder: (context) => NotFoundPage());
    }
  }
}
