package main

import (
	"cloud.google.com/go/firestore"
	"context"
	"crypto/sha256"
	"fmt"
	"github.com/jaswdr/faker"
	"gopkg.in/yaml.v3"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"time"
)

const FIREBASE_PROJECT = "runclublive"

func init() {
	if err := os.Setenv("FIRESTORE_EMULATOR_HOST", "localhost:8080"); err != nil {
		log.Fatal(err)
	}
}

func main() {

	ctx := context.Background()

	client, err := firestore.NewClient(ctx, FIREBASE_PROJECT)
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if closeRrr := client.Close(); closeRrr != nil {
			log.Fatal(closeRrr)
		}
	}()

	coaches, err := getCoaches()
	if err != nil {
		log.Fatal(err)
	}

	fkr := faker.New()

	users := make([]User, 500)
	for i := range users {
		person := fkr.Person()
		address := fkr.Address()

		users[i].FirstName = person.FirstName()
		users[i].LastName = person.LastName()
		users[i].ID = hash(users[i].FirstName + users[i].LastName)
		users[i].City = address.City()
		users[i].State = address.State()
		users[i].Country = address.CountryAbbr()
		users[i].Role = "user"
	}

	runsPerDay := 10
	daysIntoTheFuture := 7
	availableRunTimes := []time.Time{}
	minRunTimeInMinutes := 20
	maxRunTimeInMinutes := 60 * 4
	loc, err := time.LoadLocation("EST")
	if err != nil {
		log.Fatal(err)
	}
	for day := 0; day <= daysIntoTheFuture; day++ {
		for hour := 5; hour <= 17; hour++ {
			availableRunTimes = append(availableRunTimes, time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day()+day, hour, 0, 0, 0, loc))
		}
	}

	runs := make([]Run, runsPerDay*daysIntoTheFuture)
	for i := 0; i < runsPerDay*daysIntoTheFuture; i++ {
		runs[i] = Run{
			StartsAt:          randomDateElement(fkr, availableRunTimes),
			DurationInMinutes: fkr.IntBetween(minRunTimeInMinutes, maxRunTimeInMinutes),
			Title:             strings.TrimSuffix(fkr.Lorem().Sentence(3), "."),
			Description:       fkr.Lorem().Paragraph(3),
			CoachID:           randomUserElement(fkr, coaches).ID,
		}
		runs[i].userIds = make([]string, fkr.IntBetween(1, 100))
		for j := range runs[i].userIds {
			runs[i].userIds[j] = randomUserElement(fkr, users).ID
		}
		runs[i].UserCount = len(runs[i].userIds)
	}
	fmt.Println(map[string]int{
		"coaches": len(coaches),
		"users":   len(users),
		"runs":    len(runs),
	})

	for i := range coaches {
		if _, err := client.Collection("users").Doc(coaches[i].ID).Set(ctx, &coaches[i]); err != nil {
			log.Fatal(err)
		}
	}

	for i := range users {
		if _, err := client.Collection("users").Doc(users[i].ID).Set(ctx, &users[i]); err != nil {
			log.Fatal(err)
		}
	}

	for i := range runs {
		docRef, _, err := client.Collection("runs").Add(ctx, &runs[i])
		if err != nil {
			panic(err)
		}
		for j := range runs[i].userIds {
			if _, err := docRef.Collection("users").Doc(runs[i].userIds[j]).Set(ctx, RunUserData{RegisteredAt: time.Now()}); err != nil {
				log.Fatal(err)
			}
		}
	}

}

func randomUserElement(f faker.Faker, s []User) User {
	i := f.IntBetween(0, len(s)-1)
	return s[i]
}

func randomDateElement(f faker.Faker, s []time.Time) time.Time {
	i := f.IntBetween(0, len(s)-1)
	return s[i]
}

func hash(s string) string {
	h := sha256.New()
	h.Write([]byte(s))
	return fmt.Sprintf("%x", h.Sum(nil))
}

type User struct {
	ID          string `yaml:"id" firestore:"id"`
	FirstName   string `yaml:"firstName" firestore:"firstName"`
	LastName    string `yaml:"lastName" firestore:"lastName"`
	City        string `yaml:"city" firestore:"city"`
	State       string `yaml:"state" firestore:"state"`
	Country     string `yaml:"country" firestore:"country"`
	Description string `yaml:"description" firestore:"description"`
	Role        string `yaml:"role" firestore:"role"`
}

type Run struct {
	StartsAt          time.Time `firestore:"startsAt"`
	Title             string    `firestore:"title"`
	Description       string    `firestore:"description"`
	DurationInMinutes int       `firestore:"durationInMinutes"`
	CoachID           string    `firestore:"coachId"`
	UserCount         int       `firestore:"userCount"`
	userIds           []string
}

type RunUserData struct {
	RegisteredAt time.Time `firestore:"registeredAt"`
}

func getCoaches() ([]User, error) {
	b, err := ioutil.ReadFile("./coaches.yaml")
	if err != nil {
		return nil, err
	}
	type res struct {
		Coaches []User `yaml:"coaches"`
	}
	r := res{}

	if err := yaml.Unmarshal(b, &r); err != nil {
		return nil, err
	}
	for i := range r.Coaches {
		r.Coaches[i].Role = "coach"
	}
	return r.Coaches, err
}
